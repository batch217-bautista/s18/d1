// console.log("Hello World");

// [SECTION] Functions
// Parameters and Arguments

// function printInput() {
//     let nickname = prompt("Enter your nickname: ");
//     console.log("Hi " + nickname);
// }

// printInput();

// This function has parameter and argument
function printName(name) {   //name --> is our parameter
    console.log("My name is " + name);
}

printName("Juana"); // "Juana" is our argument
printName("Shawn Michael Bautista");

// 1. printName("Juana"); >> ARGUMENT
// 2. will be stored to our PARAMETER >> function printName(name)
// 3. The information or data stored in a Parameter can be used to the code block inside a function.

let sampleVariable = "Yuri";
printName(sampleVariable);

// Function argument cannot be used by a function if there are no parametes provided within the function.

function checkDivisibilityBy8(num) {
    let remainder = num % 8;
    console.log("The remainder of " + num + " divided by 8 is: " + remainder);
    let isDivisibleBy8 = remainder === 0; // the word "is..." in a function is an indication it will be boolean.
    console.log("Is " + num + " divisible by 8?");
    console.log(isDivisibleBy8)
}
checkDivisibilityBy8(32);
checkDivisibilityBy8(55);

// Can also do the same thing using prompt(), however, take note that prompt() outputs a string. Strings are not ideal for mathematical computations.

// Function as arguments
// Function parametes can also accept other functions as arguments

function argumentFunction() {
    console.log("This function was passed as an argument before the message.");
}

function invokeFunction(argumentFunction) {
    argumentFunction();
}

// Adding and removing the parentheses "()" impacts the output of JavaScript heavily
// When functin is used with parenthese "()", it denotes invoking/calling a function
// A function used without a parenthesis is normally associated with using the function as an argument to another function

invokeFunction(argumentFunction);

// or finding more information about a function in the console using console.log()

console.log(argumentFunction);

// Multiple Parameters

function createFullName(firstName, middleName, lastName) {
    console.log(firstName + ' ' + middleName + ' ' + lastName);
}

createFullName("Shawn Michael", "Lamoro", "Bautista");

// Multiple parameter using stored data in a variable
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

// Parameter names are just names to refer to the argument. Even if we change the name if the parameters, the arguments will be received in the same order it was passed.

function printFullName(middleName, firstName, lastName) {
    console.log(firstName + ' ' + middleName + ' ' + lastName);
}

printFullName("Juan", "Dela", "Cruz"); //the result is different because the console.log() arranged the letters different

// [SECTION] return statement allows us to output a value from a function to be passed to hte line/block of code that invoked/called the function.

function returnFullName(firstName, middleName, lastName) {
    return firstName + ' ' + middleName + ' ' + lastName; // "return" statement is the breaking line that indicates they have the absolute data and those below it are all ignored; hence the "Test console" statement below did not show on the results.
    console.log("Test console");
}

let completeName = returnFullName("Juan", "Dela", "Cruz");
console.log(completeName);
// In our example, the "returnFullName" function was invoked/called in the same line as declaring a variable.

// Whatever value is returned frm the "returnFullName" function is stored in the "completeName" variable

console.log(returnFullName(firstName, middleName, lastName));

function returnAddress(city, country) {
    let fullAddress = city + ", " + country;
    return fullAddress;
    console.log("This will not be displayed");
}

let myAddress = returnAddress("Cebu City", "Philippines");
console.log(myAddress);

// On the other hand, when a function that only has console.log() to display its result, it will return undefined instead.

function printPlayerInfo(username, level, job) {
    // console.log("Username " + username);
    // console.log("Level: " + level);
    // console.log("Job: " + job);
    return "Username " + username;
}

let user1 = printPlayerInfo("Knight_white", 95, "Paladin");
console.log(user1);

// returns undefined because printPlayerInfo returns nothing. It only console.logs() the details.
// You cannot save any value from printPlayerInfo() because it does not return anything.